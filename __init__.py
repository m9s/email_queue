# -*- coding: utf-8 -*-
"""
    __init__.py

"""
from trytond.pool import Pool

from . import email_queue
from . import ir

def register():
    Pool.register(
        email_queue.EmailQueue,
        ir.Cron,
        module='email_queue', type_='model'
    )
